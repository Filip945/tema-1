﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BioService.Services
{
    public class BioService : Bio.BioBase
    {
        public override Task<AddPersonResponse> AddPerson(AddPersonRequest request, ServerCallContext context)
        {
            var person = request.Person;
            
            DateTime cnpDate;
            try
            {
                cnpDate = new DateTime(Int32.Parse(person.Cnp.Substring(1, 2)) <= DateTime.Now.Year % 100 ?
                2000 + Int32.Parse(person.Cnp.Substring(1, 2)) : 1900 + Int32.Parse(person.Cnp.Substring(1, 2)),
                Int32.Parse(person.Cnp.Substring(3, 2)),
                Int32.Parse(person.Cnp.Substring(5, 2)));
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e);
                return Task.FromResult(new AddPersonResponse()
                {
                    Status = AddPersonResponse.Types.Status.Error
                });
            }
            
            String gender = new String(String.Empty);

            try
            {
                if (cnpDate.Year >= 2000)
                {
                    if (person.Cnp.ElementAt(0) != '5' && person.Cnp.ElementAt(0) != '6')
                    {
                        throw new Exception("Invalid gender digit.");
                    }
                    gender = (person.Cnp.ElementAt(0) == '5' ? "Male" : "Female");
                }
                else
                {
                    if (person.Cnp.ElementAt(0) != '1' && person.Cnp.ElementAt(0) != '2')
                    {
                        throw new Exception("Invalid gender digit.");
                    }
                    gender = (person.Cnp.ElementAt(0) == '1' ? "Male" : "Female");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return Task.FromResult(new AddPersonResponse()
                {
                    Status = AddPersonResponse.Types.Status.Error
                });
            }
            

            int age = (DateTime.Now.Month > cnpDate.Month ||
                (DateTime.Now.Month == cnpDate.Month && DateTime.Now.Day >= cnpDate.Day) ? DateTime.Now.Year - cnpDate.Year :
                DateTime.Now.Year - cnpDate.Year - 1);

            Console.WriteLine("Person Data:");
            Console.WriteLine("First Name: " + person.FirstName);
            Console.WriteLine("Last Name: " + person.LastName);
            Console.WriteLine("Gender: " + gender);
            Console.WriteLine("Age: " + age);

            return Task.FromResult(new AddPersonResponse() { Status = AddPersonResponse.Types.Status.Success,
                                    FullName = person.FirstName + " " + person.LastName, Gender = gender, Age = age});
        }
    }
}