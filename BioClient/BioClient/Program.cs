﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;

namespace BioClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Bio.BioClient(channel);

            var cancellationToken = new CancellationTokenSource(Timeout.Infinite);

            while(!cancellationToken.IsCancellationRequested)
            {
                Console.Write("First Name: ");
                var firstName = Console.ReadLine();
                Console.Write("Last Name: ");
                var lastName = Console.ReadLine();
                Console.Write("CNP: ");
                var cnp = Console.ReadLine();
                var personToAdd = new Person() { FirstName = firstName, LastName = lastName, Cnp = cnp };
                var response = await client.AddPersonAsync(new AddPersonRequest { Person = personToAdd });
                StringBuilder result = new StringBuilder();
                result.Append("\nData status: ");
                result.Append(response.Status);
                result.Append("\nFull name: ");
                result.Append(response.FullName);
                result.Append("\nGender: ");
                result.Append(response.Gender);
                result.Append("\nAge: ");
                result.Append(response.Age);
                result.Append("\n");
                Console.WriteLine(result);
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
